import sys
import re

def print_words(filename):
    text = open(filename, "r")
    d = dict()
    pattern = re.compile('[^A-Za-z ]+')
    for line in text:
        line = pattern.sub('', line)
        line = line.lower()
        for word in line.split():
            if word in d: d[word] = d[word] + 1
            else: d[word] = 1
    for key in sorted(d): print(key + " " + str(d[key]))


def print_top(filename):
    text = open(filename, "r")
    d = dict()
    pattern = re.compile('[^A-Za-z ]+')
    for line in text:
        line = pattern.sub('', line)
        line = line.lower()
        for word in line.split():
            if word in d:
                d[word] = d[word] + 1
            else:
                d[word] = 1
    d = sorted(d.items(), key=lambda item: item[1])
    i = len(d)-1
    while i > len(d)-1-20:
        print(d[i][0]+" "+str(d[i][1]))
        i = i-1


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()