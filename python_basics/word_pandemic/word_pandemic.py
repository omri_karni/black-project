"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(in_array):
    newArray = in_array.split()
    for i in range(len(newArray)):
        if (i+1) % 2 == 0:
            newArray[i] = "Corona"
    return " ".join(newArray)
    pass


# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")