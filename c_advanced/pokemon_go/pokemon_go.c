// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

// ------------------------------ functions -----------------------------
struct pokemon {
	int attack;
	int defense;
	char * effectiveness;
	char * name;
};
void winner(struct pokemon p1, struct pokemon p2)
{
	int effectiveness1;
	int effectiveness2;
	if (p1.effectiveness == "Water")
	{
		if (p2.effectiveness == "Earth")
		{
			effectiveness1 = 1;
			effectiveness2 = 3;
		}
		else if (p2.effectiveness == "Fire")
		{
			effectiveness1 = 3;
			effectiveness2 = 1;
		}
		else if (p2.effectiveness == "Air")
		{
			effectiveness1 = 2;
			effectiveness2 = 2;
		}
	}
	else if (p1.effectiveness == "Earth")
	{
		if (p2.effectiveness == "Water")
		{
			effectiveness1 = 3;
			effectiveness2 = 1;
		}
		else if (p2.effectiveness == "Fire")
		{
			effectiveness1 = 2;
			effectiveness2 = 2;
		}
		else if (p2.effectiveness == "Air")
		{
			effectiveness1 = 1;
			effectiveness2 = 3;
		}
	}
	else if (p1.effectiveness == "Fire")
	{
		if (p2.effectiveness == "Earth")
		{
			effectiveness1 = 2;
			effectiveness2 = 2;
		}
		else if (p2.effectiveness == "Water")
		{
			effectiveness1 = 1;
			effectiveness2 = 3;
		}
		else if (p2.effectiveness == "Air")
		{
			effectiveness1 = 3;
			effectiveness2 = 1;
		}
	}
	else if (p1.effectiveness == "Air")
	{
		if (p2.effectiveness == "Earth")
		{
			effectiveness1 = 3;
			effectiveness2 = 1;
		}
		else if (p2.effectiveness == "Fire")
		{
			effectiveness1 = 1;
			effectiveness2 = 3;
		}
		else if (p2.effectiveness == "Water")
		{
			effectiveness1 = 2;
			effectiveness2 = 2;
		}
	}
	float strength1 = (p1.attack / p2.defense) * effectiveness1 * 50;
	float strength2 = ((float)(p2.attack) / p1.defense) * effectiveness2 * 50;
	if (strength1 > strength2)
		printf("%s", p1.name);
	else
		printf("%s", p2.name);
}
int main()
{
	struct pokemon p1 = { 5, 3, "Air", "one" };
	struct pokemon p2 = { 4, 5, "Fire", "two" };
	winner(p1, p2);
	return (0);
}