// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// ------------------------------ functions -----------------------------
int numLength(int num)
{
	int length = 0;
	while (num != 0)
	{
		length++;
		num /= 10;
	}
	return length;
}
int numLettersCount(int num)
{
	int length = numLength(num);
	int digit;
	int letterscount = 0;
	for (int i = length ; i > 0; i--)
	{
		digit = (num) / pow(10, i - 1);
		if (i == 3)
		{
			if (digit == 1)
			{
				letterscount += 10;
				return letterscount;
			}
		}
		if (i == 2)
		{
			if (num == 10)
			{
				letterscount += strlen("ten");
				return letterscount;
			}
			if (num == 11)
			{
				letterscount += strlen("eleven");
				return letterscount;
			}
			if (num == 12)
			{
				letterscount += strlen("twelve");
				return letterscount;
			}
			if (num == 13)
			{
				letterscount += strlen("thirteen");
				return letterscount;
			}
			if (num == 14)
			{
				letterscount += strlen("fourteen");
				return letterscount;
			}
			if (num == 15)
			{
				letterscount += strlen("fifteen");
				return letterscount;
			}
			if (num == 16)
			{
				letterscount += strlen("sixteen");
				return letterscount;
			}
			if (num == 17)
			{
				letterscount += strlen("seventeen");
				return letterscount;
			}
			if (num == 18)
			{
				letterscount += strlen("eighteen");
				return letterscount;
			}
			if (num == 19)
			{
				letterscount += strlen("nineteen");
				return letterscount;
			}
			else
			{
				if (digit == 2)
					letterscount += strlen("twenty");
				if (digit == 3)
					letterscount += strlen("thirty");
				if (digit == 4)
					letterscount += strlen("fourty");
				if (digit == 5)
					letterscount += strlen("fifty");
				if (digit == 6)
					letterscount += strlen("sixty");
				if (digit == 7)
					letterscount += strlen("seventy");
				if (digit == 8)
					letterscount += strlen("eighty");
				if (digit == 9)
					letterscount += strlen("ninety");
				letterscount += 1;
			}
		}
		if (i == 1)
		{
			if (digit == 1)
				letterscount += strlen("one");
			if (digit == 2)
				letterscount += strlen("two");
			if (digit == 3)
				letterscount += strlen("three");
			if (digit == 4)
				letterscount += strlen("four");
			if (digit == 5)
				letterscount += strlen("five");
			if (digit == 6)
				letterscount += strlen("six");
			if (digit == 7)
				letterscount += strlen("seven");
			if (digit == 8)
				letterscount += strlen("eight");
			if (digit == 9)
				letterscount += strlen("nine");
		}
		num = (num) % (int)(pow(10, i-1));
	}
	return letterscount;
}

int main()
{
	int count = 0;
	for (int i = 1; i <= 100; i++)
	{
		count += numLettersCount(i);
	}
	printf("%d", count);
	return (0);
}