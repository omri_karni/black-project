// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

// ------------------------------ functions -----------------------------
struct rectangle {
	int height;
	int width;
};
int * answer(struct rectangle r)
{
	int * answer = (int *)malloc(100); //����� ����� �� ������� ������� ������
	int answerpos = 0; // ������ �� ���� ��� ��� ������� �����
	struct rectangle cur_rec = { r.height, r.width };
	int max_height;
	while (cur_rec.width * cur_rec.height != 0) // ��� ��� ����� ������� ������ �� ���� ���� ����� ����� �� ����� ���
	{
		if (cur_rec.width > cur_rec.height) // �� ����� ����� ����
		{
			answer[answerpos] = cur_rec.height;
			cur_rec.width -= cur_rec.height; //����� ����� ������
		}
		else if (cur_rec.width < cur_rec.height) // �� ����� ���� ����
		{
			answer[answerpos] = cur_rec.width;
			cur_rec.height -= cur_rec.width; // ����� ����� ������
		}
		else //�� �� �����
		{
			answer[answerpos] = cur_rec.height;
			cur_rec.height = 0;
			cur_rec.width = 0;
		}
		answerpos += 1;
	}
	answer[answerpos] = '\0';
	return answer;
}

int main()
{
	struct rectangle r1 = { 5,3 };
	int *p = answer(r1);
	int i = 0;
	while (p[i] != '\0')
	{
		printf("%d, ", p[i]);
		i++;
	}
	return (0);
}