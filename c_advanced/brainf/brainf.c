// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

// ------------------------------ functions -----------------------------
int getChunkSize(char * code)
{
	int pos = 0;
	int bracketcount = 0;
	do {
		if (code[pos] == '[')
			bracketcount++;
		if (code[pos] == ']')
			bracketcount--;
		pos++;
	} while (bracketcount != 0);
	return pos;
}
void brainf(char * ptr, char * code)
{
	char c;
	char * codepos = code;
	while (*codepos != 'EOF')
	{
		c = *codepos;
		if (c == ']')
			return;
		if (c == '>')
			++ptr;
		if (c == '<')
			--ptr;
		if (c == '+')
			++*ptr;
		if (c == '-')
			--*ptr;
		if (c == '.')
			putchar(*ptr);
		if (c == '[')
		{
			while (*ptr > 0)
			{
				brainf(ptr, codepos + 1); //���� ������ ����� ���� �����
			}
			int size = getChunkSize(codepos);
			for (int i = 0; i < size - 1; i++)
				++codepos;
		}
		++codepos;
	}
	free(code);
}
int main(int argc, char * argv[])
{
	if (argc < 2)
	{
		printf("missing file_path parameter.\n");
		return EXIT_FAILURE;
	}

	FILE * f = fopen(argv[1], "r");
	char * ptr = (char *)calloc(1000, sizeof(char));
	char * code = (char *)malloc(sizeof(char) * sizeof(f));
	char ** code2 = &code;
	fscanf(f, "%s", code);
	brainf(ptr, code);
	fclose(f);

	return (0);
}