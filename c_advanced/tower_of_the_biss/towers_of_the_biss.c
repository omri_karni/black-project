// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

// ------------------------------ functions -----------------------------
struct node {
	int num;
	struct node * next;
};
struct stack {
	char name[2];
	struct node * top;
};

void push(int num, struct stack * s)
{
	//����� ��� �������
	struct node * new_node = (struct node *)malloc(sizeof(struct node));
	new_node->num = num;
	if (s->top == NULL)
	{
		s->top = new_node;
	}
	else
	{
		struct node * temp = (struct node *)malloc(sizeof(struct node));
		temp = s->top;
		new_node->next = temp;
		s->top = new_node;
	}
}

struct node * pop(struct stack * s)
{
	//����� ��� �������
	if (s->top->next == NULL)
		s->top = NULL;
	else
	{
		struct node * temp = s->top;
		s->top = temp->next;
		temp->next = NULL;
	}
}

void move(int num_of_disks, struct stack * disk_pole, struct stack * aid_pole, struct stack * target_pole)
{
	if (num_of_disks == 1)//��  ���� ����� �� ������ ���
	{
		printf("Move disk %d from %c to %c\n", disk_pole->top->num, disk_pole->name[0], target_pole->name[0]);
		push((pop(disk_pole))->num, target_pole); // ����� ������� ����� �����
	}
	else
	{
		move(num_of_disks - 1, disk_pole, target_pole, aid_pole); // ����� �� �������� ��� �������� ����� ���
		move(1, disk_pole, aid_pole, target_pole); // ����� ������� ������� ����� �����
		move(num_of_disks - 1, aid_pole, disk_pole, target_pole); // ����� �� �������� ���� �� ���� ���� ����� �����
	}
}

int main()
{
	printf("insert number of disks\n");
	int num_of_disks; //����� ���� ��������
	scanf("%d", &num_of_disks);
	int i = num_of_disks;
	struct stack * s1 = (struct stack *)malloc(sizeof(struct stack)); //����� ����� ������
	s1->name[0] = 'A';
	s1->name[1] = '\0';
	while (i != 0) //���� �������� ����� ������
	{
		push(i, s1);
		i -= 1;
	}
	struct stack * s2 = (struct stack *)malloc(sizeof(struct stack)); // ����� ����� ����
	s2->top = NULL;
	s2->name[0] = 'B';
	s2->name[1] = '\0';
	struct stack * s3 = (struct stack *)malloc(sizeof(struct stack)); // ����� ����� ������
	s3->top = NULL;
	s3->name[0] = 'C';
	s3->name[1] = '\0';
	move(num_of_disks, s1, s2, s3);
	return (0);
}