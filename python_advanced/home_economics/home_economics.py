"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    # startPriceOld: המחיר ההתחלתי של הרכב שלו
    # startPriceNew: המחיר ההתחלתי של הרכב שהוא רוצה לקנות
    # savingPerMonth: כמה כסף דוד משה חוסך בחודש
    # percentLossByMonth: אחוזי ירידת הערך של רכבים (האחוז ההתחלתי!)
    currentSaving = 0  # כמה כסף יש לדוד משה
    currentMonth = 0  # מספר החודש הנוכחי
    if startPriceOld + currentSaving >= startPriceNew:
        return (currentMonth, round(startPriceOld + currentSaving - startPriceNew))
    else:
        currentMonth = currentMonth + 1 #אחרי חודש אחד
        currentSaving = currentSaving + savingPerMonth
        startPriceOld = startPriceOld - (startPriceOld * percentLossByMonth / 100)
        startPriceNew = startPriceNew - (startPriceNew * percentLossByMonth / 100)

        while startPriceOld + currentSaving < startPriceNew:
            currentMonth = currentMonth+1 #אחרי חודשיים
            if currentMonth % 2 == 0: percentLossByMonth = percentLossByMonth + 0.5
            currentSaving = currentSaving + savingPerMonth
            startPriceOld = startPriceOld - (startPriceOld * percentLossByMonth / 100)
            startPriceNew = startPriceNew - (startPriceNew * percentLossByMonth / 100)
        return (currentMonth, round(startPriceOld + currentSaving - startPriceNew))

    pass


# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
