"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def dejumble(word, potentional_words):
    acceptedWords = []
    for potential_word in potentional_words:
        accepted = True
        temp_word = word
        for letter in potential_word:
            if letter not in temp_word: accepted = False
            else:
                temp_word= temp_word[:temp_word.find(letter)] + temp_word[temp_word.find(letter)+1:]
        if accepted: acceptedWords.append(potential_word)
    return acceptedWords
    pass


# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
