import string
"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""


def crypto_breaker(file_path):
    text = open(file_path, "r")
    text = text.read()
    coded_chars = text.split(",") #רשימת התווים המוצפנים
    keys = [] #התווים המשומשים להצפנה
    key_denied = False
    for num in range(3):
        for possible_key in string.ascii_lowercase:
            key_denied = False
            i = len(keys)
            while i < len(coded_chars) and not key_denied:
                decoded_char = chr(ord(possible_key) ^ int(coded_chars[i]))
                if not decoded_char.isprintable() and ord(decoded_char) != 10:
                    key_denied = True
                i += 3
            if not key_denied:
                keys.append(possible_key)
                break
    decoded_text= ""
    for i in range(len(coded_chars)):
        decoded_char = chr(int(coded_chars[i]) ^ int(ord(keys[i % 3])))
        if ord(decoded_char) == 10:
            decoded_text += "\n"
        else:
            decoded_text += decoded_char
    print(decoded_text)
    pass


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker('big_secret.txt')
