"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""


def nevigate_validate(path):
    y_pos = x_pos = 0
    beginning_x_pos = beginning_y_pos = 0 #נקודות התחלת המסלול
    for line_num in range(len(path)):
        if "X" in path[line_num]:
            y_pos = line_num
            beginning_y_pos = y_pos
            x_pos = path[line_num].find("X")
            beginning_x_pos = x_pos
            break
    first_check = second_check = None
    valids = {"-": ["-", "+", "X"], "|": ["|", "+", "X"], "+_horizontal": ["+", "-", "X"],
              "+_vertical": ["+", "|", "X"], "X": ["X", "-", "|", "+"]}
    for i in range(2):
        location_history = []
        action_history = []
        if first_check is False:
            for line_num in range(len(path)):
                if "X" in path[line_num]: #מציאת הX השני
                    if beginning_y_pos != line_num and beginning_x_pos != path[line_num].find("X"):
                        y_pos = line_num
                        beginning_y_pos = y_pos
                        x_pos = path[line_num].find("X")
                        beginning_x_pos = x_pos
        while first_check is not True and second_check is None:
            sub_x = sub_y = add_x = add_y = True  # האם ערכי הx והy הם הערכים הראשונים או האחרונים בשורה ובטור
            if x_pos == 0:
                sub_x = False
            if x_pos == len(path[y_pos]) - 1:
                add_x = False
            if y_pos == 0:
                sub_y = False
            if y_pos == len(path) - 1:
                add_y = False

            current_sign = path[y_pos][x_pos]  # הסימון הנוכחי
            if [y_pos, x_pos] in location_history: return False  # אם הובל פעמיים לאותו המקום
            location_history.append([y_pos, x_pos])  # הוספת המיקום לרשימת המיקומים
            if current_sign == "X":
                start_has_one_sign = None
                if len(action_history) != 0:  # אם נמצא הX השני
                    if first_check is None:
                        first_check = True
                        continue
                    elif second_check is None:
                        second_check = True
                        break
                if add_x:
                    if path[beginning_y_pos][beginning_x_pos + 1] in valids["X"]:
                        if start_has_one_sign is None:
                            start_has_one_sign = True
                            x_pos = x_pos + 1
                            action_history.append("Right")
                        else:
                            start_has_one_sign = False
                if sub_x:
                    if path[beginning_y_pos][beginning_x_pos - 1] in valids["X"]:
                        if start_has_one_sign is None:
                            start_has_one_sign = True
                            x_pos = x_pos - 1
                            action_history.append("Left")
                        else:
                            start_has_one_sign = False
                if add_y:
                    if path[beginning_y_pos + 1][beginning_x_pos] in valids["X"]:
                        if start_has_one_sign is None:
                            start_has_one_sign = True
                            y_pos = y_pos + 1
                            action_history.append("Down")
                        else:
                            start_has_one_sign = False
                if sub_y:
                    if path[beginning_y_pos - 1][beginning_x_pos] in valids["X"]:
                        if start_has_one_sign is None:
                            start_has_one_sign = True
                            y_pos = y_pos - 1
                            action_history.append("Up")
                        else:
                            start_has_one_sign = False
                if not start_has_one_sign: return False  # אם כמה דרכים יוצאות מההתחלה
            elif current_sign == "-":  # ימינה או שמאלה
                if action_history[-1] == "Right":
                    if path[y_pos][x_pos + 1] in valids["-"]:
                        x_pos = x_pos + 1
                        action_history.append("Right")
                    else:
                        if first_check is None:
                            first_check = False
                            continue
                        elif second_check is None:
                            return False
                elif action_history[-1] == "Left":
                    if path[y_pos][x_pos - 1] in valids["-"]:
                        x_pos = x_pos - 1
                        action_history.append("Left")
                    else:
                        if first_check is None:
                            first_check = False
                            continue
                        elif second_check is None:
                            return False
            elif current_sign == "|":  # ירידה או עלייה
                if action_history[-1] == "Down":
                    if path[y_pos + 1][x_pos] in valids["|"]:
                        y_pos = y_pos + 1
                        action_history.append("Down")
                    else:
                        if first_check is None:
                            first_check = False
                            continue
                        elif second_check is None:
                            return False
                elif action_history[-1] == "Up":
                    if path[y_pos - 1][x_pos] in valids["|"]:
                        y_pos = y_pos - 1
                        action_history.append("Up")
                    else:
                        if first_check is None:
                            first_check = False
                            continue
                        elif second_check is None:
                            return False
            elif current_sign == "+":  # צומת
                if action_history[-1] == "Left" or action_history[-1] == "Right":
                    if add_y:
                        if sub_y:
                            if path[y_pos + 1][x_pos] in valids["+_vertical"] and path[y_pos - 1][x_pos] in valids["+_vertical"]:  # אם יוצאות כמה דרכים מצומת
                                if first_check is None:
                                    first_check = False
                                    continue
                                elif second_check is None:
                                    return False
                        if path[y_pos + 1][x_pos] in valids["+_vertical"]:
                            y_pos = y_pos + 1
                            action_history.append("Down")
                            continue
                    if sub_y:
                        if path[y_pos - 1][x_pos] in valids["+_vertical"]:
                            y_pos = y_pos - 1
                            action_history.append("Up")
                            continue
                elif action_history[-1] == "Down" or action_history[-1] == "Up":
                    if add_x:
                        if sub_x:
                            if path[y_pos][x_pos + 1] in valids["+_horizontal"] and path[y_pos][x_pos - 1] in valids["+_horizontal"]:  # אם יוצאות כמה דרכים מצומת
                                if first_check is None:
                                    first_check = False
                                    break
                                elif second_check is None:
                                    return False
                        if path[y_pos][x_pos + 1] in valids["+_horizontal"]:
                            x_pos = x_pos + 1
                            action_history.append("Right")
                            continue
                    if sub_x:
                        if path[y_pos][x_pos - 1] in valids["+_horizontal"]:
                            x_pos = x_pos - 1
                            action_history.append("Left")
                            continue
        if first_check: break

    signs_locations = []
    for line_num in range(len(path)):
        for letter_num in range(len(path[line_num])):
            if path[line_num][letter_num] != " ":
                signs_locations.append([line_num, letter_num]) #מציאת כל הסימנים במפה והכנסתם לרשימה
    for sign_location in signs_locations:
        if sign_location not in location_history: return False #אם יש סימנים צפים


    return True

# small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]

    valid = nevigate_validate(path)

    path = ["           ",
            "X--|-+-    "
            "      -    "
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
