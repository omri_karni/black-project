import math
"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""


def ejected():
    actions= {"UP": 1, "DOWN": -1, "RIGHT": 1, "LEFT": -1}
    action = input()
    position = [0, 0]
    while action != "0":
        actionDirection = action[0: action.find(" ")]
        actionValue = int(action[action.find(" "): ])
        if actionDirection == "UP" or actionDirection == "DOWN":
            position[1] = position[1] + actions[actionDirection] * actionValue
        if actionDirection == "RIGHT" or actionDirection == "LEFT":
            position[0] = position[0] + actions[actionDirection] * actionValue
        action = input()
    return round(math.sqrt(pow(position[0], 2) + pow(position[1], 2)))
    pass


# small test to check it's work.
if __name__ == '__main__':
    print(ejected())
