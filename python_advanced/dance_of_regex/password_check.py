import string
"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""


def validate_passwords(passwords):
    special_letters = "@#$%^&*"
    passwords_list = passwords.split(', ')
    accepted_passwords = []
    for password in passwords_list:
        if len(password) >= 6 and len(password) <= 12:
            hasLowerCase = hasUpperCase = hasNumber = hasSpecial = denied = False
            for letter in password:
                if letter in string.ascii_uppercase: hasUpperCase = True
                elif letter in string.ascii_lowercase: hasLowerCase = True
                elif letter.isdigit(): hasNumber = True
                elif letter in special_letters: hasSpecial = True
                else: denied = True
            if not denied:
                if hasLowerCase and hasUpperCase and hasNumber and hasSpecial: accepted_passwords.append(password)
    print (", ".join(accepted_passwords))
    pass


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()
