/**
 * @file getline.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that read lines from FILE (file or stdin).
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ------------------------------ functions -----------------------------
size_t my_getline(char ** lineptr, size_t * n, FILE * stream)
{
	size_t size = 0; //���� ����� ������
	int n2 = *n; // ���� ����� �� �����
	char c; // �� ����� �� ��� 
	while ((c = fgetc(stream)) != EOF)
	{
		if (c == '\n') // �� ��� �����
		{
			*(*lineptr + size) = '\0';
			return size;

		}
		if (size >= n2) // �� ���� ����� ���� ���� ����� �����
		{
			*lineptr = (char *)realloc(*lineptr, size * 2); // ����� �����
			n2 = size * 2;
		}
		*(*lineptr + size) = c;
		size = size + 1;
	}

    return size;
}

int main()
{
    char * buffer;
    size_t bufsize = 5;
    size_t characters;

    buffer = (char *) malloc(bufsize * sizeof(char));
    if( buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
	characters = my_getline(&buffer, &bufsize, stdin);
	//buffer = (char *)realloc(buffer, 5);
    printf("%zu characters were read.\n", characters);
	//fwrite(buffer, sizeof(char), characters, stdout);
    printf("You typed: '%s'\n", buffer);
	free(buffer);

    return(0);
}