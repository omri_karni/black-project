/**
 * @file word_count.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */


// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

// ------------------------------ functions -----------------------------
long int findSize(char file_name[])
{
	//���� ���� ����� �����
	FILE * fptr = fopen(file_name, "r");
	if (fptr == NULL) {
		printf("File Not Found!\n");
		return -1;
	}
	fseek(fptr, 0L, SEEK_END);
	long int size = ftell(fptr);
	fclose(fptr);
	return size;
}

int main(int argc, char * argv[])
{
	
    size_t word_count = 0;
	
    // you can add more initial checks
    if (argc < 2)
    {
        printf("missing file_path parameter.\n");
        return EXIT_FAILURE;
    }
	
    // open the file and count how many word are in it.
	long int size = findSize(argv[1]);
	FILE * fptr = fopen(argv[1], "r");
	bool valid_word = true; //�� �� ������ ��������
	bool has_characters = false; //�� ����� ���� ����� ������
	for (int i = 0; i < size; i++)
	{
		char c = fgetc(fptr);
		if (c == ' ')
		{
			if (valid_word && has_characters)
				word_count = word_count + 1;
			valid_word = true;
			has_characters = false;
		}
		else
		{
			if (isalpha(c))
			{
				has_characters = true;
			}
			else
				valid_word = false;
		}
	}


    printf("The file %s contain %u words.\n", argv[1], word_count);

	return 0;
}