/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ------------------------------ functions -----------------------------
char * my_strstr(const char * a, const char * b)
{
	char * longer;
	char * shorter;
	if (strlen(a) > strlen(b))
	{
		longer =  a;
		shorter = b;
	}
	else
	{
		longer = b;
		shorter = a;
	}
	int i = 0;
	int x = 0;
	while (i < strlen(longer) && x < strlen(shorter))
	{
		if (longer[i] == shorter[x])
			x += 1;
		else
			x = 0;
		i += 1;
	}
	if (x == strlen(shorter))
		return shorter;
	else
	{
		return "None";
	}
}

int main(int argc, char * argv[])
{
   char s1[] = "AAAAAAACCCAAAAAAAA";
   char s2[] = "CCC";

   char * ret = my_strstr(s1, s2);

   printf("The substring is: %s\n", ret);

	return 0;
}